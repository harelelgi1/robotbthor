package frc.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.photonvision.EstimatedRobotPose;
import org.photonvision.PhotonCamera;
import org.photonvision.PhotonPoseEstimator;
import org.photonvision.PhotonPoseEstimator.PoseStrategy;
import org.photonvision.targeting.PhotonPipelineResult;
import org.photonvision.targeting.PhotonTrackedTarget;

import edu.wpi.first.apriltag.AprilTagFieldLayout;
import edu.wpi.first.apriltag.AprilTagFieldLayout.OriginPosition;
import edu.wpi.first.apriltag.AprilTagFields;
import edu.wpi.first.math.Pair;
import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.subsystems.Swerve.SwerveSubsystem;

public class Vision {
  // The field layout
  private AprilTagFieldLayout m_aprilTagFieldLayout;
  // A list of the cameras on the robot
  private VisionCamera m_camera;
  private ShuffleboardTab m_shuffleboardTab;
  private GenericEntry m_visionTestDriveEntry;
  private GenericEntry m_visionTestVisionEntry;
  private GenericEntry m_visionTestDiffEntry;
  private GenericEntry m_visionTestPercentDiffEntry;

  /**
   * Creates a new instance of Vision
   * Sets up field layout, and cameras
   * @param shuffleboardTab The vision shuffleboard tab
   * @param camList The list of camera names and their translation from the center of the robot
   */
  public Vision(String name , Transform3d pose) {

    try {
      // Try to find the field layout
      m_aprilTagFieldLayout = AprilTagFields.k2024Crescendo.loadAprilTagLayoutField();
    } catch (Exception e) {
      // If it can't find it, use the layout in the constants
      m_aprilTagFieldLayout = new AprilTagFieldLayout(Constants.FieldConstants.APRIL_TAGS, Constants.FieldConstants.kFieldLength, Constants.FieldConstants.kFieldWidth);
      DriverStation.reportWarning("Could not find k2023ChargedUp.m_resourceFile, check that GradleRIO is updated to at least 2023.2.1 in build.gradle",  e.getStackTrace());
    }

    // if(DriverStation.getAlliance() == Alliance.B)
    // Sets the origin to the right side of the blue alliance wall
    m_aprilTagFieldLayout.setOrigin(OriginPosition.kBlueAllianceWallRightSide);

    // Puts the cameras in an array list
    // for (int i = 0; i < camList.size(); i++) {
    //   m_cameras.add(this.new VisionCamera(camList.get(i).getFirst(), camList.get(i).getSecond()));
    // }
    
    m_camera = new VisionCamera(name,  pose);

    
  }

  /**
   * Returns where it thinks the robot is
   * @param referencePose The pose to use as a reference, usually the previous robot pose
   * @return An array list of estimated poses, one for each camera that can see an april tag
   */
  public ArrayList<EstimatedRobotPose> getEstimatedPoses(Pose2d referencePose) {
    ArrayList<EstimatedRobotPose> estimatedPoses = new ArrayList<>();
    Optional<EstimatedRobotPose> estimatedPose = m_camera.getEstimatedPose(referencePose);
    
      // If the camera can see an april tag that exists, add it to the array list
      // April tags that don't exist might return a result that is present but doesn't have a pose
      if (estimatedPose.isPresent() && estimatedPose.get().estimatedPose != null) {
        estimatedPoses.add(estimatedPose.get());
        LogManager.addDoubleArray("Vision/camera/estimated pose2d", new double[] {
          estimatedPose.get().estimatedPose.getX(),
          estimatedPose.get().estimatedPose.getY(),
          estimatedPose.get().estimatedPose.getRotation().getZ()
        });
      }
    return estimatedPoses;
  }

  /**
   * Gets the pose as a Pose2d
   * @param referencePoses The reference poses in order of preference, null poses will be skipped
   * @return The pose of the robot, or null if it can't see april tags
   */
  public Pose2d getPose2d(Pose2d... referencePoses){
    Pose2d referencePose = new Pose2d();
    for (Pose2d checkReferencePose:referencePoses){
      if (checkReferencePose != null) {
        referencePose = checkReferencePose;
        break;
      }
    }
    ArrayList<EstimatedRobotPose> estimatedPoses = getEstimatedPoses(referencePose);
    // Translation2d translation = new Translation2d();
    // double rotation = 0;
    
    if (estimatedPoses.size() == 1) return estimatedPoses.get(0).estimatedPose.toPose2d();
    
    if (estimatedPoses.size() == 2) {
      return new Pose2d(
        estimatedPoses.get(0).estimatedPose.toPose2d().getTranslation()
          .plus(estimatedPoses.get(1).estimatedPose.toPose2d().getTranslation())
          .div(2),
        
          new Rotation2d(
            // Functions.modulusMidpoint(
            estimatedPoses.get(0).estimatedPose.toPose2d().getRotation().getRadians(),
            estimatedPoses.get(1).estimatedPose.toPose2d().getRotation().getRadians()
          //   -Math.PI,
          //    Math.PI)
          )
      );
    }
          
    //TODO: VERY LOW PRIORITY FOR FUTURE ROBOTS, make the rotation average work with more than 2 cameras
    // for(int i = 0; i < estimatedPoses.size(); i ++){
    //   translation=translation.plus(estimatedPoses.get(i).estimatedPose.toPose2d().getTranslation());
    // }

    // if(posesUsed>0){
    //   return new Pose2d(translation.div(estimatedPoses.size()), new Rotation2d());
    // }
    return null;
  }

  public AprilTagFieldLayout getAprilTagFieldLayout(){
    return m_aprilTagFieldLayout;
  }

  /**
   * Gets the pose of an april tag
   * @param id AprilTag id (1-8)
   * @return Pose3d of the AprilTag
   */
  public Pose3d getTagPose(int id){
    if(id < 1 || id > 16){
      System.out.println("Tried to find the pose of april tag "+id);
      return null;
    }
    
    return getAprilTagFieldLayout().getTagPose(id).get();
  }

  public List<PhotonTrackedTarget>  getIDList(){
    return m_camera.getLatestResults().getTargets();
}
  
  public void setupVisionShuffleboard() {
    GenericEntry visionTestDriveEntry = m_shuffleboardTab.add("Distance Test Drive Distance", 0).getEntry();
    GenericEntry visionTestVisionEntry = m_shuffleboardTab.add("Distance Test Vision Distance", 0).getEntry();
    GenericEntry visionTestDiffEntry = m_shuffleboardTab.add("Distance Test Difference", 0).getEntry();
    GenericEntry visionTestPercentDiffEntry = m_shuffleboardTab.add("Distance Test % Difference", 0).getEntry();
  }
  
  public class VisionCamera {
    static PhotonCamera camera;
    PhotonPoseEstimator photonPoseEstimator;
  
    /**
     * Stores information about a camera
     * @param cameraName The name of the camera on PhotonVision
     * @param robotToCam The transformation from the robot to the camera
     */
    public VisionCamera(String cameraName, Transform3d robotToCam) {
      camera = new PhotonCamera(cameraName);
      photonPoseEstimator = new PhotonPoseEstimator(
        m_aprilTagFieldLayout, 
        PoseStrategy.AVERAGE_BEST_TARGETS, 
        camera, 
        robotToCam
      );
      
      photonPoseEstimator.setMultiTagFallbackStrategy(PoseStrategy.AVERAGE_BEST_TARGETS);
      photonPoseEstimator.setReferencePose(new Pose2d());
    }
  
    /**
     * Gets the estimated pose from the camera
     * @param referencePose Pose to use for reference, usually the previous estimated robot pose
     * @return estimated robot pose
     */
    public Optional<EstimatedRobotPose> getEstimatedPose(Pose2d referencePose) {
      photonPoseEstimator.setReferencePose(referencePose);
      
      PhotonPipelineResult cameraResult = camera.getLatestResult();
      // if there is a target detected and not in the past, 
      // check the ambiguity isn't too high
      if (cameraResult.hasTargets() && cameraResult.getTimestampSeconds() > 0) {

          // go through all the targets
          List<PhotonTrackedTarget> targetsUsed = cameraResult.targets;
          for (int i = 0; i < targetsUsed.size(); i++) {
            // check their ambiguity, if it is above the highest wanted amount, return nothing

            // System.out.println("bla bla bla: " +targetsUsed.get(i).getPoseAmbiguity() + " v: " + targetsUsed.get(i).toString());
            if (targetsUsed.get(i).getPoseAmbiguity() > 1.02) {
              return Optional.empty();
            }
          }
        
      }

      Optional<EstimatedRobotPose> pose = photonPoseEstimator.update(cameraResult);

      return pose;
    } 

    public static PhotonPipelineResult getLatestResults(){
      return camera.getLatestResult();
    }

    public static boolean hasResults(){
      return camera.hasTargets();
    }
  }


  public void addTestCommands(ShuffleboardTab testTab, GenericEntry testEntry, SwerveSubsystem drive){

    testTab.add("Align to 0 degrees", new TestVisionAlignment(0, drive, this));
    testTab.add("Align to 90 degrees", new TestVisionAlignment(Math.PI/2, drive, this));
    testTab.add("Align to -90 degrees", new TestVisionAlignment(-Math.PI/2, drive, this));
    testTab.add("Align to 180 degrees", new TestVisionAlignment(Math.PI, drive, this));
  }

  public GenericEntry getVisionTestDriveEntry() {
    return m_visionTestDriveEntry;
  }

  public GenericEntry getVisionTestVisionEntry() {
    return m_visionTestVisionEntry;
  }

  public GenericEntry getVisionTestDiffEntry() {
    return m_visionTestDiffEntry;
  }

  public GenericEntry getVisionTestPercentDiffEntry() {
    return m_visionTestPercentDiffEntry;
  }

  public void updateOdometry(SwerveDrivePoseEstimator poseEstimator){
    // The current position as a translation
    Translation2d currentEstimatedPoseTranslation = poseEstimator.getEstimatedPosition().getTranslation();
    // An array list of poses returned by different cameras
    ArrayList<EstimatedRobotPose> estimatedPoses = getEstimatedPoses(poseEstimator.getEstimatedPosition());
    for (EstimatedRobotPose estimatedPose : estimatedPoses) {
        // The position of the closest april tag as a translation
        Translation2d closestTagPoseTranslation = null;
        for (int j = 0; j < estimatedPose.targetsUsed.size(); j++) {
          // System.out.println(estimatedPose.targetsUsed.get(j).getArea() + " label area");
          if(estimatedPose.targetsUsed.get(j).getArea() > 0.13){
            // The position of the current april tag
            Pose3d currentTagPose = getTagPose(estimatedPose.targetsUsed.get(j).getFiducialId());
            // If it can't find the april tag's pose, don't run the rest of the for loop for this tag
            if (currentTagPose == null) {
                continue;
            }
           //  System.out.println(estimatedPose.estimatedPose.toString());
            Translation2d currentTagPoseTranslation = currentTagPose.toPose2d().getTranslation();

            // If the current april tag position is closer than the closest one, this makes makes it the closest
            if (closestTagPoseTranslation == null || currentEstimatedPoseTranslation.getDistance(currentTagPoseTranslation) < currentEstimatedPoseTranslation.getDistance(closestTagPoseTranslation)) {
                closestTagPoseTranslation = currentTagPoseTranslation;
            }
        }else{
            return;
        }}

        double visionFactor = (currentEstimatedPoseTranslation.getDistance(closestTagPoseTranslation) * Constants.VisionConstants.kVisionPoseStdDevFactor);
        

        // Adds the vision measurement for this camera
        poseEstimator.addVisionMeasurement(
                estimatedPose.estimatedPose.toPose2d(),
                estimatedPose.timestampSeconds,
                Constants.VisionConstants.kBaseVisionPoseStdDevs.plus(
                        visionFactor
                                                           )
                                          );
        if (Constants.logging) {
            LogManager.addDouble("Vision/ClosestTag Distance",
                                 currentEstimatedPoseTranslation.getDistance(closestTagPoseTranslation)
                                );
        }
      }
    }
  }


