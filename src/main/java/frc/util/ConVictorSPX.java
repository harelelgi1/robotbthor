package frc.util;

import com.ctre.phoenix.ParamEnum;
import com.ctre.phoenix.motorcontrol.ControlFrame;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.LimitSwitchNormal;
import com.ctre.phoenix.motorcontrol.LimitSwitchSource;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

public class ConVictorSPX {
    final static int kTimeoutMs = 1000;
    final static Configuration config = new Configuration();
    public ConVictorSPX() {

    }


    public static VictorSPX createVictor(int id) {
        VictorSPX victor = new VictorSPX(id);
        victor.configFactoryDefault(1000);
        victor.set(ControlMode.PercentOutput, 0.0);
      
        victor.changeMotionControlFramePeriod(config.MOTION_CONTROL_FRAME_PERIOD_MS);
        victor.clearMotionProfileHasUnderrun(kTimeoutMs);
        victor.clearMotionProfileTrajectories();
      
        victor.clearStickyFaults(kTimeoutMs);
      
        victor.configSetParameter(
                ParamEnum.eClearPositionOnLimitF, 0, 0, 0, kTimeoutMs);
        victor.configSetParameter(
                ParamEnum.eClearPositionOnLimitR, 0, 0, 0, kTimeoutMs);
      
        victor.configNominalOutputForward(0, kTimeoutMs);
        victor.configNominalOutputReverse(0, kTimeoutMs);
        victor.configNeutralDeadband(config.NEUTRAL_DEADBAND, kTimeoutMs);
      
      
        victor.configVelocityMeasurementPeriod(config.VELOCITY_MEASUREMENT_PERIOD, kTimeoutMs);
        victor.configVelocityMeasurementWindow(config.VELOCITY_MEASUREMENT_ROLLING_AVERAGE_WINDOW,
                kTimeoutMs);
      
        victor.configOpenloopRamp(config.OPEN_LOOP_RAMP_RATE, kTimeoutMs);
        victor.configClosedloopRamp(config.CLOSED_LOOP_RAMP_RATE, kTimeoutMs);
      
        victor.configVoltageCompSaturation(0.0, kTimeoutMs);
        victor.configVoltageMeasurementFilter(32, kTimeoutMs);
        victor.enableVoltageCompensation(false);
      
      
        victor.setStatusFramePeriod(StatusFrame.Status_1_General,
            config.GENERAL_STATUS_FRAME_RATE_MS, kTimeoutMs);
        victor.setStatusFramePeriod(StatusFrame.Status_2_Feedback0,
            config.FEEDBACK_STATUS_FRAME_RATE_MS, kTimeoutMs);

        victor.setStatusFramePeriod(StatusFrame.Status_4_AinTempVbat,
            config.ANALOG_TEMP_VBAT_STATUS_FRAME_RATE_MS, kTimeoutMs);

        victor.setControlFramePeriod(ControlFrame.Control_3_General, config.CONTROL_FRAME_PERIOD_MS);
      
        return victor;
      }
      
}
