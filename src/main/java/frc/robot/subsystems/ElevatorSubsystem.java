package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.wpilibj.DMASample.DMAReadStatus;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.util.Falcon500;

public class ElevatorSubsystem extends SubsystemBase {
  TalonFX elevator;
  TalonFX climb;
  TalonFX slide;
  TalonFX intake;
  

  public ElevatorSubsystem() {
    //Elevator Confings
    elevator = Falcon500.createTalon( Constants.ElevatorConstants.ElevatorId, Constants.ElevatorConstants.ElevatorNeutralMode);
    elevator.config_kP(0,0.1);
    elevator.config_kI(0,Constants.ElevatorConstants.ElevatorKi);
    elevator.config_kD(0, Constants.ElevatorConstants.ElevatorKd);
    elevator.config_kF(0, Constants.ElevatorConstants.ElevatorKf);
    elevator.configAllowableClosedloopError(0, Constants.ElevatorConstants.ElevatorAllowableError);
    elevator.configClosedLoopPeakOutput(0, Constants.ElevatorConstants.ElevatorPeakOutput);


    //climb Confings 
    climb = Falcon500.createTalon(Constants.ElevatorConstants.ClimbID,Constants.ElevatorConstants.ClimbNeutralMode);
    climb.config_kP(0,Constants.ElevatorConstants.ClimbKp);
    climb.config_kI(0,Constants.ElevatorConstants.ClimbKi);
    climb.config_kD(0, Constants.ElevatorConstants.ClimbKd);
    climb.config_kF(0, Constants.ElevatorConstants.ClimbKf);
    climb.configAllowableClosedloopError(0, Constants.ElevatorConstants.ClimbAllowableError);
    climb.configClosedLoopPeakOutput(0, Constants.ElevatorConstants.ClimbPeakOutput);

    

    //slide Configs
    slide = Falcon500.createTalon(Constants.ElevatorConstants.SlideID,Constants.ElevatorConstants.SlideNeutralMode);
    slide.config_kP(0,Constants.ElevatorConstants.SlideKp);
    slide.config_kI(0,Constants.ElevatorConstants.SlideKi);
    slide.config_kD(0, Constants.ElevatorConstants.SlideKd);
    slide.config_kF(0, Constants.ElevatorConstants.SlideKf);
    slide.configAllowableClosedloopError(0, Constants.ElevatorConstants.SlideAllowableError);
    slide.configClosedLoopPeakOutput(0, Constants.ElevatorConstants.SlidePeakOutput);


    intake = Falcon500.createTalon(Constants.ElevatorConstants.IntakeID, NeutralMode.Brake);
  }

  public void setElevatorPower(double power) {
    elevator.set(ControlMode.PercentOutput, power);
  }

  public void setClimbPower(double power) {
    climb.set(ControlMode.PercentOutput, power);
  }

  public void setSlide(double power) {
    slide.set(ControlMode.PercentOutput, power);
  }
  public void setIntake(double power) {
    intake.set(ControlMode.PercentOutput, power);
  }
  public void setElevatorPose(double position){
    elevator.set(ControlMode.Position,position);
  }

  public void setSlidePosition(double position){
    slide.set(ControlMode.Position,position);
  }
  public double getSlidePosition(){
    return slide.getSelectedSensorPosition();
  }
  public void setClimbPosition(double position) {
    climb.set(ControlMode.Position,position);
  }
  public double getClimbPosition(){
    return climb.getSelectedSensorPosition();
  }
  public boolean isClimbAtPosition(double error, double target){
    return target>getClimbPosition() - error && getClimbPosition()<target + error;
  }
  public boolean isSlideAtPosition(double error,double target){
    return target>getSlidePosition() - error&& getSlidePosition()>target +error;

  }

  public double getElevatorPosition() {
    return elevator.getSelectedSensorPosition();
  }

  public boolean isElevatorInError(double error, double target) {
    return target > getElevatorPosition() - error && target < getElevatorPosition() + error;
  }

  @Override
  public void periodic() {
  }
}
