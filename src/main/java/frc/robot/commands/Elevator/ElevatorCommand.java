// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Elevator;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ElevatorSubsystem;

public class ElevatorCommand extends Command {
  ElevatorSubsystem elevator;
  Joystick joystick;

  public ElevatorCommand(ElevatorSubsystem elevator, Joystick joystick) {
    this.elevator = elevator;
    this.joystick = joystick;
    addRequirements(elevator);
    
    
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(joystick.getRawAxis(5)>0.2){
        elevator.setElevatorPower(0.5);
    }else if(joystick.getRawAxis(5)<-0.2){
     elevator.setElevatorPower(-0.5);
    }else{
      elevator.setElevatorPower(0);
    }
    elevator.setElevatorPower(joystick.getRawAxis(5));
    elevator.setClimbPower(joystick.getRawAxis(1) * 0.3);

    if(joystick.getRawButton(7)) {
      elevator.setSlide(0.15);
    }else if(joystick.getRawButton(8)){
      elevator.setSlide(-0.15);
    }else {
      elevator.setSlide(0);
    }

    if(joystick.getRawButton(3)) {
      elevator.setIntake(-0.5);
      
    }else if(joystick.getRawButton(2)) {
      elevator.setIntake(1);
    }else {
      elevator.setIntake(0);
    } 
   

  }

  @Override
  public void end(boolean interrupted) {}

  @Override
  public boolean isFinished() {
    return false;
  }
}
