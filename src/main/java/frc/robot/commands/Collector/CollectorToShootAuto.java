// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Collector;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.shooterSubsystem;

public class CollectorToShootAuto extends Command {
  shooterSubsystem shooter;
  Timer time;
  
  public CollectorToShootAuto(shooterSubsystem shooter) {
    this.shooter = shooter;
    time = new Timer();

    addRequirements(shooter);
  }

  @Override
  public void initialize() {
    time.stop();
    time.reset();
    time.start();
  }

  @Override
  public void execute() {
    // shooter.shooter(0.4,0.4);
    // shooter.delivery(0.16);
    shooter.shooter(0.4,0.4);
    shooter.delivery(0.3);
    shooter.setArmPosition(190000);
    SmartDashboard.putBoolean("Is note in code command", shooter.isNoteInCollector());
 }

  @Override
  public void end(boolean interrupted) {
    shooter.shooter(0,0);
    shooter.delivery(0);


    time.stop();
    time.reset();

  }

  @Override
  public boolean isFinished() {
    return shooter.isNoteInCollector() || time.hasElapsed(2);
  }
}