// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.Collector;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.subsystems.shooterSubsystem;

public class ShootNoneDistance extends Command {
  shooterSubsystem shooterSubsystem;
  Timer time;
  Timer deliverTimer;
  boolean hasDelivered = false;
  double velocity;
  double position;
  public ShootNoneDistance(shooterSubsystem shooterSubsystem, double velocity, double position) {
    time = new Timer();
    deliverTimer = new Timer();
    this.shooterSubsystem = shooterSubsystem;
    this.velocity = velocity;
    this.position = position;
    addRequirements(shooterSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    time.stop();
    time.reset();
    deliverTimer.stop();
    deliverTimer.reset();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    System.out.println("getting inside shooter");
    shooterSubsystem.shooterVelocity(velocity , velocity);
    shooterSubsystem.setArmPosition(position);
    if(shooterSubsystem.isArmAtPosition() && shooterSubsystem.reachedVelocity()) { 
      time.start();
      if(time.hasElapsed(0.3)) {
        shooterSubsystem.delivery(-0.6);
        System.out.println("spinning");
        hasDelivered = true;
        deliverTimer.start();
      }
    }else {
      time.reset();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    shooterSubsystem.shooter(0,0);
    shooterSubsystem.delivery(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return deliverTimer.hasElapsed(0.5);
  }
}
