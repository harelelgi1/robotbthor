
package frc.robot.commands.Collector;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.shooterSubsystem;

public class SetShooterToPosition extends Command {
  shooterSubsystem shooter;
  double position;
  public SetShooterToPosition(shooterSubsystem shooter, double position) {
    this.position = position;
    this.shooter = shooter;
    addRequirements(shooter);
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    shooter.setArmPosition(position);
  }

  @Override
  public void end(boolean interrupted) {
    System.out.println("fniished shooter position");
  }

  @Override
  public boolean isFinished() {
    return shooter.isArmAtPosition(10000,position);
  }
}
