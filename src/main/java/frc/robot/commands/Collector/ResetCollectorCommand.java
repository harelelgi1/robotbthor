package frc.robot.commands.Collector;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.subsystems.shooterSubsystem;

public class ResetCollectorCommand extends Command {
  shooterSubsystem shooterSubsystem;
  public ResetCollectorCommand(shooterSubsystem collecShooterSubsystem) {
      this.shooterSubsystem = collecShooterSubsystem;

      addRequirements(collecShooterSubsystem);
  }

  @Override
  public void initialize() {
    shooterSubsystem.moveArm(0);
    shooterSubsystem.shooter(0,0);
    shooterSubsystem.delivery(0);
  }

  @Override
  public void execute() {
    shooterSubsystem.setArmPosition(10000);
  }

  @Override
  public void end(boolean interrupted) {
    shooterSubsystem.shooter(0,0);
    shooterSubsystem.delivery(0);
    shooterSubsystem.moveArm(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return (shooterSubsystem.getArmPosition() > 
                      Constants.CollectorConstants.ClosingPosition - Constants.CollectorConstants.ArmAllowableError ) 
          && (shooterSubsystem.getArmPosition() < 
                      Constants.CollectorConstants.ClosingPosition + Constants.CollectorConstants.ArmAllowableError);
  }
}
