// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.crypto.Data;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.fasterxml.jackson.core.JacksonException;
import com.revrobotics.ColorSensorV3;

import edu.wpi.first.util.struct.parser.ParseException;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.shooterSubsystem;
import frc.robot.subsystems.Swerve.SwerveSubsystem;
import frc.util.PS5ControllerDriverConfig;

public class ShooterDefaultCommand extends Command {
  shooterSubsystem shooter;
  SwerveSubsystem swerve;
  Joystick joystick;

  List<Data> dataList = new ArrayList<>();

  String path = "C:\\Users\\User\\Downloads\\robotbthor-odometry\\robotbthor\\src\\main\\deploy\\ShootingData.json";
  JSONObject json = new JSONObject();
  JSONArray dataArray = new JSONArray();

  double ShooterAngle , ShooterVelocity;

  Timer timer;
  public ShooterDefaultCommand(shooterSubsystem shooter , SwerveSubsystem swerve,Joystick joystick) {
    this.ShooterAngle = 0.0;
    this.ShooterVelocity = 0.0;
    this.shooter = shooter;
    this.joystick = joystick;
    this.swerve = swerve;

    timer = new Timer();
    addRequirements(shooter);
  }

  @Override
  public void initialize() {
    timer.stop();
    timer.reset();
  }

  @Override
  public void execute() {
    if (joystick.getRawAxis(1)>0.2) {
      shooter.moveArm(0.1);
    }else if(joystick.getRawAxis(1)<-0.2){
      shooter.moveArm(-0.1);
    }else{
      shooter.moveArm(0);
    }

    // if (joystick.getRawButton(2)){
    //   if(shooter.getIntakeSpeeds() > Math.abs(shooter.getShooterVelocityFromDashBoard()- 300) && shooter.getIntakeSpeeds() < Math.abs(shooter.getShooterVelocityFromDashBoard() + 300)) { 
    //     // shooter.delivery(-0.3);
    //     timer.start();
    //     if(timer.hasElapsed(  0.5)) {
    //       shooter.delivery(-0.3);
    //       // System.out.println("dafadsf" + timer.get());
    //     }else {
    //       shooter.delivery(0);
    //     }
    //   }else {
    //     timer.reset();
    //   }
    //   shooter.shooterVelocity(shooter.getShooterVelocityFromDashBoard(), shooter.getShooterVelocityFromDashBoard());

    // }
     if(joystick.getRawButton(6)){     
        shooter.shooterVelocity(16000 , 16000);
        // shooter.setArmPosition(shooter.getShootingFunction(swerve.getDistanceFromSpeaker()));

        System.out.println(" arm " + shooter.isArmAtPosition()  + " shooter" + shooter.reachedVelocity());
        if(
          // shooter.isArmAtPosition() && 
        shooter.reachedVelocity()) { 
        // shooter.delivery(-0.3);
        timer.start();
        
        if(timer.hasElapsed(  0.3)) {
          shooter.delivery(-0.3);
          // System.out.println("dafadsf" + timer.get());
        }else {
          shooter.delivery(0);
        }
      }else {
        timer.reset();
      }
    }else if(joystick.getRawButton(4)) {
      shooter.shooter(0.3,0.3);
      shooter.delivery(0.3);
    }else {
      shooter.shooter(0,0);
      shooter.delivery(0);
      timer.reset();
    }
  }
    
    // if(joystick.getRawButtonPressed(14)){
    //   shooter.setAllSettingsForShooter(
    //         shooter.getShooterKpFromShuffleBoard(), 
    //         shooter.getShooterKiFromShuffleBoard(), 
    //         shooter.getShooterKdFromShuffleBoard(), 
    //         shooter.getShooterKfFromShuffleBoard());
    // }
    // SmartDashboard.putNumber("Shooter angle", ShooterAngle);
    // SmartDashboard.putNumber("Shooter velocity", ShooterVelocity);

    // if(joystick.getRawButton(3)) {
    //   shooter.delivery(-0.2);
    // }else {
    //   shooter.delivery(0);
    // }
    // timer.delay(0.002);

 
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}

