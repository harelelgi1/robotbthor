// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.units.Angle;
import edu.wpi.first.units.Time;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.Swerve.SwerveSubsystem;

public class TurnTo extends Command {
  SwerveSubsystem drive;
  double angle;
  Timer time = new Timer();
  public TurnTo(SwerveSubsystem drive , double angle) {
    this.drive = drive;
    this.angle = angle;

  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    drive.stop();
    time.reset();
    time.start();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    drive.driveHeadingAuto(0, 0, angle, true);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return drive.getYaw().getDegrees() > (angle - 3) && time.hasElapsed(3);
  }
}
